/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.radd.samplecrud.controllers;

import io.github.radd.samplecrud.models.User;
import io.github.radd.samplecrud.models.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Piotr
 */
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MainServlet at " + request.getContextPath() + "</h1>");
            
//            User u1 = new User( 0, "lord", "pass1", "lord@email.com", "text");
//            User u2 = new User( 0, "lord2", "pass2", "lord2@email.com", "text2");
//            User u3 = new User( 0, "lord3", "pass3", "lord3@email.com", "text3");
//            User u4 = new User( 0, "lord4", "pass4", "lord4@email.com", "text4");
            
            UserDAO userDAO = new UserDAO();
//            userDAO.addUser(u1);
//            userDAO.addUser(u2);
//            userDAO.addUser(u3);
//            userDAO.addUser(u4);
             
            //if(userDAO.removeUser(22))
            //   System.out.println("Usunięto");
            //userDAO.removeUser(20);
            
            //User userUp = new User(2, "newName", "", "newEmail@email.com", "my new desc");
            //if(userDAO.updateUser(userUp))
            //   System.out.println("User update");
                
            List<User> users = userDAO.getAllUsers();
            
            if(users != null) {
                for(User u : users)
                    out.println(u.toString() + "<br>");
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        UserDAO userDAO = new UserDAO();
        List<User> users = userDAO.getAllUsers();
        request.setAttribute("users", users);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


}
