/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.radd.samplecrud.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Piotr
 */
public class UserDAO {

    private Connection connection;
    private Statement stmt;
    public static final String TABLE_NAME = "user";

    public UserDAO() {
        DBConnection con = new DBConnection();
        try {
            connection = con.getConnection();
            stmt = connection.createStatement();
            createTable();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void createTable() throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
                + "("
                + "userID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "username varchar(100),"
                + "password varchar(100),"
                + "email varchar(100),"
                + "description varchar(500)"
                + ")";

        stmt.execute(sql);
    }
    
    public boolean addUser(User user) {
        try {
            PreparedStatement pStmt = connection.prepareStatement(
                    "INSERT INTO " + TABLE_NAME + " VALUES (NULL, ?,?,?,?);");
            pStmt.setString(1, user.getUsername());
            pStmt.setString(2, user.getPassword());
            pStmt.setString(3, user.getEmail());
            pStmt.setString(4, user.getDescription());
            pStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean removeUser(int userID) {
        try {
            PreparedStatement pStmt = connection.prepareStatement(
                    "DELETE FROM " + TABLE_NAME + " WHERE userID = ?");
            pStmt.setInt(1, userID);
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean updateUser(User user) {
        try {
            PreparedStatement pStmt = connection.prepareStatement(
                    "UPDATE " + TABLE_NAME + " SET username = ?,  email = ?, description = ? "
                    + "WHERE userID = ?");
            pStmt.setString(1, user.getUsername());
            pStmt.setString(2, user.getEmail());
            pStmt.setString(3, user.getDescription());
            pStmt.setInt(4, user.getUserID());
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public User getUserByID(int id) {
        List<User> users = new LinkedList<>();
        try {
            PreparedStatement pStmt = connection.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME +
                    " WHERE userID = ?");
            pStmt.setInt(1, id);
            ResultSet res = pStmt.executeQuery();
            while (res.next()) {
                User user = new User();
                user.setUserID(res.getInt("userID"));
                user.setUsername(res.getString("username"));
                user.setPassword(res.getString("password"));
                user.setEmail(res.getString("email"));
                user.setDescription(res.getString("description"));
                users.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return users.get(0);
    }
    
    public List<User> getAllUsers() {
        List<User> users = new LinkedList<>();
        try {
            ResultSet res = stmt.executeQuery("SELECT * FROM " + TABLE_NAME);
            while (res.next()) {
                User user = new User();
                user.setUserID(res.getInt("userID"));
                user.setUsername(res.getString("username"));
                user.setPassword(res.getString("password"));
                user.setEmail(res.getString("email"));
                user.setDescription(res.getString("description"));
                users.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return users;
    }

}
