/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.radd.samplecrud.models;

/**
 *
 * @author Piotr
 */
public class User {
    
    private int userID;
    private String username;
    private String password;
    private String email;
    private String description;

    public User() {
    }

    public User(int userID, String username, String password, String email, String descryption) {
        this.userID = userID;
        this.username = username;
        this.password = password;
        this.email = email;
        this.description = descryption;
    }

    /**
     * @return the userID
     */
    public int getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the descryption
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the descryption to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "userID: " + userID +
                ", username: " + username +
                ", password: " + password + 
                ", email: " + email +
                ", desc: " + description;
    }

    
    
}
