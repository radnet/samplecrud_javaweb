<%-- 
    Document   : index
    Created on : 25-Aug-2018, 11:21:00
    Author     : Piotr
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Title</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/lib/bootstrap/css/bootstrap.min.css" />" rel="stylesheet" >

    <!-- Custom styles for this template -->
    <link href="<c:url value="/css/style.css" />" rel="stylesheet">
</head>
<body>
    
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">Start Bootstrap</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron my-4">
            <h1 class="display-3">${ dane }</h1>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
            <a href="#" class="btn btn-primary btn-lg">Call to action!</a>
        </header>

    </div>
    <!-- /.container -->
    
     <div class="container">
        <a href="<c:url value="/add" />" class="btn btn-primary btn">Add user</a>
        <br>
        <br>
     </div>
    <div class="container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">User ID</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Description</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                <c:forEach items="${users}" var="user" varStatus="loop">
                    <tr>
                      <th scope="row">${loop.index+1}</th>
                      <td>${user.userID}</td>
                      <td>${user.username}</td>
                      <td>${user.email}</td>
                      <td>${user.description}</td>
                      <td>
                            <a href="<c:url value="/edit?id=${user.userID}" />" class="btn btn-light btn-sm" role="button">Edit</a>
                            <a href="<c:url value="/delete?id=${user.userID}" />" class="btn btn-light btn-sm" role="button">Delete</a>
                      </td>
                    </tr>
                </c:forEach>
            </tbody>
          </table>
    </div>
    <!-- Footer -->
    <footer class="">
        <div class="container">
            <p>Copyright &copy; Your Website 2018</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
